const User = require('../models/User');

const userCtrl ={};

userCtrl.puntoC = async (req, res) => {
    
    var pipeline = [
        {
            "$project": {
                "_id": 0.0,
                "name": 1.0,
                "seguidores": {
                    "$size": "$followers"
                }
            }
        }, 
        {
            "$sort": {
                "seguidores": -1.0
            }
        }, 
        {
            "$limit": 5
        }
    ];

    const userInstanceList = await User.aggregate(pipeline);
    res.json(userInstanceList);
}

userCtrl.puntoG = async (req, res) => {
    
    var pipeline = [
        {
            "$project": {
                "_id": 0.0,
                "name": 1.0,
                "seguidores": {
                    "$size": "$following"
                }
            }
        }, 
        {
            "$sort": {
                "seguidores": -1.0
            }
        }, 
        {
            "$lookup": {
                "from": "users",
                "let": {
                    "cant": "$seguidores"
                },
                "pipeline": [
                    {
                        "$project": {
                            "_id": 0.0,
                            "name": 1.0,
                            "seguidores": {
                                "$size": "$following"
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": null,
                            "promedio": {
                                "$avg": "$seguidores"
                            }
                        }
                    },
                    {
                        "$project": {
                            "_id": 0.0
                        }
                    }
                ],
                "as": "fromItems"
            }
        }, 
        {
            "$unwind": "$fromItems"
        }, 
        {
            "$match": {
                "$expr": {
                    "$gt": [
                        "$seguidores",
                        "$fromItems.promedio"
                    ]
                }
            }
        }, 
        {
            "$project": {
                "_id": 0.0,
                "name": 1.0,
                "seguidores": 1.0,
                "promedio": "$fromItems.promedio"
            }
        }
    ];

    const userInstanceList = await User.aggregate(pipeline);
    res.json(userInstanceList);
}

userCtrl.getInstance = async (req, res) => {
    const userInstance = await User.findById(req.params.id);
    res.json(userInstance);
}

userCtrl.createInstance = async (req, res) => {
    const userInstance = new User(req.body);
    await userInstance.save();
    res.json({
        'status': 'Usuario guardado'
    });
}

userCtrl.editInstance = async (req, res) => {
    await User.findByIdAndUpdate(req.params.id, {$set: req.body});
    res.json({
        'status': 'Usuario guardado'
    });
}

userCtrl.deleteInstance = async (req, res) => {
    await User.findByIdAndRemove(req.params.id);
    res.json({
        'status': 'Usuario eliminado'
    });
}

userCtrl.login = async (req, res) => {

    var userInstance = await User.findOne({'email': req.body.email});
    if(!userInstance){
        userInstance = new User(req.body);
        userInstance = await userInstance.save();
    }
    
    res.json(userInstance);
    
}

module.exports = userCtrl;