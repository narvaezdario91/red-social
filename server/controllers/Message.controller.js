const Message = require('../models/Message');

const mongoose = require('mongoose');

const messageCtrl ={};

messageCtrl.getList = async (req, res) => {
    const messageInstanceList = await Message.find();
    res.json(messageInstanceList);
}

messageCtrl.puntoB = async (req, res) => {

    var pipeline = [
        {
            "$match": {
                "$and": [
                    {
                        "replicated": {
                            "$exists": true
                        }
                    },
                    {
                        "replicated.date": {
                            "$gte": "2020-01-01T00:00:00.000000Z"
                        }
                    },
                    {
                        "replicated.date": {
                            "$lt": "2021-01-01T00:00:00.000000Z"
                        }
                    }
                ]
            }
        }, 
        {
            "$group": {
                "_id": {_id:"$replicated._id", message: "$replicated.message"},
                "numberReplicates": {
                    "$sum": 1.0
                }
            }
        }, 
        {
            "$match": {
                "numberReplicates": {
                    "$gte": 3.0
                }
            }
        }
    ];

    const messageInstanceList = await Message.aggregate(pipeline);
    res.json(messageInstanceList);
}

messageCtrl.puntoF = async (req, res) => {

    var pipeline = [
        {
            "$match":{
               "date":{
                  "$gte": new Date("2020-01-01T00:00:00.000Z"),
                  "$lt": new Date("2021-01-01T00:00:00.000Z")
               }
            }
         },
         {
            "$group":{
               "_id":{
                  "month":{
                     "$substr":[
                        "$date",
                        5,
                        2
                     ]
                  },
                  "username":"$user.username"
               },
               "numberMessages":{
                  "$sum":1
               }
            }
         },
         {
            "$group":{
               "_id":{
                  "month":"$_id.month"
               },
               "maxValue":{
                  "$max":"$numberMessages"
               },
               "users":{
                  "$push":{
                     "$cond":[
                        {
                           "$eq":[
                              "$numberMessages",
                              {
                                 "$max":"$numberMessages"
                              }
                           ]
                        },
                        {
                           "username":"$_id.username"
                        },
                        "$$REMOVE"
                     ]
                  }
               }
            }
         },
         {
            "$sort":{
               "_id.month":1
            }
         }
      
    ];

    const messageInstanceList = await Message.aggregate(pipeline);
    res.json(messageInstanceList);
}

messageCtrl.puntoD = async (req, res) => {

    var pipeline = [
        {
            "$group": {
                "_id": {
                    "nombre": "$user.name",
                    "mes": {
                        "$toInt": {
                            "$dateToString": {
                                "format": "%Y%m",
                                "date": "$date"
                            }
                        }
                    }
                },
                "total": {
                    "$sum": 1.0
                }
            }
        }, 
        {
            "$group": {
                "_id": {
                    "nombre": "$_id.nombre"
                },
                "promedio": {
                    "$avg": "$total"
                }
            }
        }, 
        {
            "$sort": {
                "_id.nombre": 1.0
            }
        }, 
        {
            "$project": {
                "_id": "$_id.nombre",
                "promedio": 1.0
            }
        }
    ];

    const messageInstanceList = await Message.aggregate(pipeline);
    res.json(messageInstanceList);
}

messageCtrl.puntoE = async (req, res) => {

    var pipeline = [
        {
            "$project": {
                "_id": 0.0,
                "user": 1.0,
                "_id2": {
                    "$toString": "$_id"
                }
            }
        }, 
        {
            "$lookup": {
                "from": "messages",
                "let": {
                    "nid": "$_id2"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$eq": [
                                    "$forwarded._id",
                                    "$$nid"
                                ]
                            }
                        }
                    }
                ],
                "as": "fromItems"
            }
        }, 
        {
            "$project": {
                "_id": 1.0,
                "user": 1.0,
                "cant": {
                    "$size": "$fromItems"
                }
            }
        }, 
        {
            "$group": {
                "_id": {
                    "nombre": "$user.name",
                    "usuario": "$user.username"
                },
                "total": {
                    "$sum": 1.0
                },
                "reenviados": {
                    "$sum": {
                        "$cond": [
                            {
                                "$gt": [
                                    "$cant",
                                    0.0
                                ]
                            },
                            1.0,
                            0.0
                        ]
                    }
                }
            }
        }, 
        {
            "$project": {
                "_id": 0.0,
                "nombre": "$_id.nombre",
                "usuario": "$_id.usuario",
                "total": 1.0,
                "reenviados": 1.0,
                "promedio": {
                    "$divide": [
                        "$reenviados",
                        "$total"
                    ]
                }
            }
        }, 
        {
            "$sort": {
                "promedio": -1.0
            }
        }, 
        {
            "$limit": 1.0
        }
    ];

    const messageInstanceList = await Message.aggregate(pipeline);
    res.json(messageInstanceList);
}

messageCtrl.puntoA = async (req, res) => {

    const messageReplicatedList = await Message.find({ 
        "replicated" : { 
            "$ne" : null
        }
    }, 
    { 
        "_id" : "$replicated._id"
    });

    let messageReplicatedListId = messageReplicatedList.map(s => mongoose.Types.ObjectId(s._id));

    var pipeline = [
        {
            "$match": {
                "$and": [
                    {
                        "date": {
                            "$gte": new Date("2019-12-31 19:00:00.000-05:00"),
                            "$lt": new Date("2020-12-31 19:00:00.000-05:00")
                        }
                    },{
                        "replicated": {
                            "$exists": false
                        }
                    },
                    {
                        "forwarded": {
                            "$exists": false
                        }
                    },
                    {
                        "_id": {$nin: messageReplicatedListId}
                    }
                ]
            }
        }
    ];

    const messageOriginalList = await Message.aggregate(pipeline);

    console.log(messageOriginalList.length);

    res.json(messageOriginalList);
}
messageCtrl.getInstance = async (req, res) => {
    const messageInstance = await Message.findById(req.params.id);
    res.json(messageInstance);
}

messageCtrl.createInstance = async (req, res) => {
    const messageInstance = new Message(req.body);
    await messageInstance.save();
    res.json({
        'status': 'Usuario guardado'
    });
}

messageCtrl.editInstance = async (req, res) => {
    await Message.findByIdAndUpdate(req.params.id, {$set: req.body});
    res.json({
        'status': 'Usuario guardado'
    });
}

messageCtrl.deleteInstance = async (req, res) => {
    await Message.findByIdAndRemove(req.params.id);
    res.json({
        'status': 'Usuario eliminado'
    });
}

messageCtrl.login = async (req, res) => {

    var messageInstance = await Message.findOne({'email': req.body.email});
    if(!messageInstance){
        messageInstance = new Message(req.body);
        messageInstance = await messageInstance.save();
    }
    
    res.json(messageInstance);
    
}

module.exports = messageCtrl;