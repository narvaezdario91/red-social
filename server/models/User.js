const mongoose = require('mongoose');
const{Schema} = mongoose;

const UserSchema = new Schema({
    username: String,
    name: String,
    passwd: String,
    location:{
        country: String,
        city: String
    },
    userType: Number,
    following: [String],
    followers: [String],
    top:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "messages"
        }
    ]
    
});

module.exports = mongoose.model('users', UserSchema);