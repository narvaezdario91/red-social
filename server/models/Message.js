const mongoose = require('mongoose');
const{Schema} = mongoose;

const MessageSchema = new Schema({
    date: Date,
    message: String,
    user:{
        username: String,
        user: String
    },
    forwarded:{
        _id: String,
        date: Date,
        message: String,
        user:{
            username: String,
            user: String
        }
    },
    replicated:{
        _id: String,
        date: Date,
        message: String,
        user:{
            username: String,
            user: String
        }
    }
    
});

module.exports = mongoose.model('messages', MessageSchema);