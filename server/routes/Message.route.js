const express = require('express');
const router = express.Router();

const messageCtrl = require('../controllers/Message.controller');

router.get('', messageCtrl.getList);
//router.get('/:id', messageCtrl.getInstance);
router.post('', messageCtrl.createInstance);
router.put('/:id', messageCtrl.editInstance);
router.delete('/:id', messageCtrl.deleteInstance);

router.get('/puntoB/', messageCtrl.puntoB);
router.get('/puntoF/', messageCtrl.puntoF);
router.get('/puntoD/', messageCtrl.puntoD);
router.get('/puntoE/', messageCtrl.puntoE);
router.get('/puntoA/', messageCtrl.puntoA);

module.exports = router;