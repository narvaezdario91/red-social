import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PuntoAComponent } from './components/punto-a/punto-a.component';
import { PuntoBComponent } from './components/punto-b/punto-b.component';
import { PuntoCComponent } from './components/punto-c/punto-c.component';
import { PuntoDComponent } from './components/punto-d/punto-d.component';
import { PuntoEComponent } from './components/punto-e/punto-e.component';
import { PuntoFComponent } from './components/punto-f/punto-f.component';
import { PuntoGComponent } from './components/punto-g/punto-g.component';

@NgModule({
  declarations: [
    AppComponent,
    PuntoAComponent,
    PuntoBComponent,
    PuntoCComponent,
    PuntoDComponent,
    PuntoEComponent,
    PuntoFComponent,
    PuntoGComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
