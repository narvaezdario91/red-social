import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntoFComponent } from './punto-f.component';

describe('PuntoFComponent', () => {
  let component: PuntoFComponent;
  let fixture: ComponentFixture<PuntoFComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuntoFComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntoFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
