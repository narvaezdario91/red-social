import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-punto-f',
  templateUrl: './punto-f.component.html',
  styleUrls: ['./punto-f.component.css']
})
export class PuntoFComponent implements OnInit {

  public data:any = []
  constructor(private http: HttpClient) {
   
  }

  ngOnInit(): void {
    const url ='http://localhost:3000/api/messages/puntoF'
    this.http.get(url).subscribe((res)=>{
      this.data = res
      console.log(this.data)
    })
  }

}
