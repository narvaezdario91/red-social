import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-punto-g',
  templateUrl: './punto-g.component.html',
  styleUrls: ['./punto-g.component.css']
})
export class PuntoGComponent implements OnInit {

  public data:any = []
  constructor(private http: HttpClient) {
   
  }

  ngOnInit(): void {
    const url ='http://localhost:3000/api/users/puntoG'
    this.http.get(url).subscribe((res)=>{
      this.data = res
      console.log(this.data)
    })
  }

}
