import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntoGComponent } from './punto-g.component';

describe('PuntoGComponent', () => {
  let component: PuntoGComponent;
  let fixture: ComponentFixture<PuntoGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuntoGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntoGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
