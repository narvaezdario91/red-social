import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-punto-e',
  templateUrl: './punto-e.component.html',
  styleUrls: ['./punto-e.component.css']
})
export class PuntoEComponent implements OnInit {

  public data:any = []
  constructor(private http: HttpClient) {
   
  }

  ngOnInit(): void {
    const url ='http://localhost:3000/api/messages/puntoE'
    this.http.get(url).subscribe((res)=>{
      this.data = res
      console.log(this.data)
    })
  }

}
