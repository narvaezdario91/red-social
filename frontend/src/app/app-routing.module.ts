import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PuntoAComponent } from './components/punto-a/punto-a.component';
import { PuntoBComponent } from './components/punto-b/punto-b.component';
import { PuntoCComponent } from './components/punto-c/punto-c.component';
import { PuntoDComponent } from './components/punto-d/punto-d.component';
import { PuntoEComponent } from './components/punto-e/punto-e.component';
import { PuntoFComponent } from './components/punto-f/punto-f.component';
import { PuntoGComponent } from './components/punto-g/punto-g.component';

const routes: Routes = [
  { path: '', component: PuntoAComponent},
  { path: 'puntoA', component: PuntoAComponent},
  { path: 'puntoB', component: PuntoBComponent},
  { path: 'puntoC', component: PuntoCComponent},
  { path: 'puntoD', component: PuntoDComponent},
  { path: 'puntoE', component: PuntoEComponent},
  { path: 'puntoF', component: PuntoFComponent},
  { path: 'puntoG', component: PuntoGComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
